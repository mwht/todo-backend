package org.example.repository;

import org.example.model.TodoList;
import org.example.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoListRepository extends JpaRepository<TodoList, Integer> {

    /**
     * Zwraca listę todo-list, które należą do usera.
     * Tworzenie zapytań springowych opisałem w UserRepository
     *
     * @param owner obiekt usera
     * @return lista todo-list, które należą do podanego usera
     */
    List<TodoList> findByOwner(User owner);

}
