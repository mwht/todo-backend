package org.example.repository;


import org.example.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    /**
     * Znajduje użytkownika po kolumnie "username".
     * Spring automatycznie wygeneruje tą metodę.
     * findNazwaModeluByNazwaKolumny(TypKolumny kryterium);
     * findNazwaModeluByNazwaKolumnyAndNazwaKolumny2(TypKolumny kryterium, TypKolumny2 kryterium2);
     * itd.
     *
     * Tutaj dokładniej jest opisane skąd się bierze nazwy tych metod:
     * https://www.baeldung.com/spring-data-derived-queries
     */
    Optional<User> findUserByEmail(String email);
}
