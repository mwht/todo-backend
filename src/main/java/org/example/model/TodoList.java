package org.example.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class TodoList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    /**
     * \@ElementCollection określa typ kolekcji, do jakiej odnosi się to pole
     * Spring Data JPA nie zna typu String jako \@Entity, więc trzeba mu pomóc
     * i pokazać że mapujemy zwykłe Stringi.
     */
    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    private List<String> items;
    @OneToOne(fetch = FetchType.EAGER)
    private User owner;

    /**
     * To jest potrzebne do obsługi JSONa w Springu - bez tego wywala że nie ma domyślnego konstruktora,
     * więc musisz taki domyślny konstruktor zrobić - nie ważne co tam będzie, ważne żeby jakkolwiek
     * inicjowało pola w Entity
     */
    public TodoList() {
        this(null, null, null, null);
    }

    public TodoList(Integer id, String title, List<String> items, User owner) {
        this.id = id;
        this.title = title;
        this.items = items;
        this.owner = owner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
