package org.example.model;


import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "auth_user")
public class User {

	@Id
	@GeneratedValue(strategy =  GenerationType.AUTO)
	@Column(name = "auth_user_id")
	private Integer id;

	@NotNull(message = "Email is compulsory")
	@Email(message = "Email is invalid")
	@Column(name = "email", unique=true)
	private String email;

	@NotNull(message="Password is compulsory")
	@Length(min=5, message="Password should be at least 5 characters")
	@Column(name = "password")
	private String password;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "auth_user_role", joinColumns = @JoinColumn(name = "auth_user_id"), inverseJoinColumns = @JoinColumn(name = "auth_role_id"))
	private Set<Role> roles = new HashSet<Role>();

	@ManyToOne(cascade = CascadeType.ALL)
	private User user_id;

	public User() {
		this(null, null, null, null, null);
	}

	public User(Integer id, @NotNull(message = "Email is compulsory") @Email(message = "Email is invalid") String email, @NotNull(message = "Password is compulsory") @Length(min = 5, message = "Password should be at least 5 characters") String password, Set<Role> roles, User user_id) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.roles = roles;
		this.user_id = user_id;
	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public User getUser_id() {
		return user_id;
	}

	public void setUser_id(User user_id) {
		this.user_id = user_id;
	}
}
