package org.example.service;

import org.example.model.TodoList;
import org.example.model.User;
import org.example.repository.TodoListRepository;
import org.example.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TodoListAccessService {

    @Autowired
    private TodoListRepository todoListRepository;

    @Autowired
    private UserRepository userRepository;

    public boolean canUserAccessTodoList(String email, TodoList todoList) {
        return todoList != null && (isUserAdmin(email) || todoList.getOwner().getEmail().equals(email));
    }

    public boolean canUserAccessTodoList(String email, Integer todoListId) {
        if (todoListId == null) {
            return false;
        }

        Optional<TodoList> todoList = todoListRepository.findById(todoListId);
        return todoList.isPresent() && (todoList.get().getOwner().getEmail().equals(email) || isUserAdmin(email));
    }

    private boolean isUserAdmin(String email) {
        Optional<User> userOptional = userRepository.findUserByEmail(email);
        if(!userOptional.isPresent()) {
            return false;
        }

        User user = userOptional.get();
        return user.getRoles().stream().anyMatch(role -> role.getRole().equals("ADMIN_USER"));
    }
}
