package org.example.service;


import org.example.model.User;

public interface UserService {

	public User saveUser(User user);
	public boolean isUserAlreadyPresent(User user);
	public User getLoggedInUser();
}
