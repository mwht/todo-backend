package org.example.controller;

import org.example.model.TodoList;
import org.example.model.User;
import org.example.repository.TodoListRepository;
import org.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
public class TodoController {

    @Autowired
    UserService userService;

    @Autowired
    TodoListRepository todoListRepository;

    @GetMapping("/todo")
    public List<TodoList> getListOfTodoItems() {
        User loggedInUser = userService.getLoggedInUser();
        if (loggedInUser == null) {
            return null;
        }

        return todoListRepository.findByOwner(loggedInUser);
    }

    @GetMapping("/todo/{id}")
    @PreAuthorize("@todoListAccessService.canUserAccessTodoList(authentication.principal, #id)")
    public ResponseEntity<?> getTodoList(@PathVariable("id") Integer id) {
        Optional<TodoList> todoList = todoListRepository.findById(id);
        if (todoList.isPresent()) {
            return ResponseEntity.of(todoList);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/todo/{id}")
    @PreAuthorize("@todoListAccessService.canUserAccessTodoList(authentication.principal, #id)")
    public ResponseEntity<?> updateTodoList(@PathVariable("id") Integer id, @RequestBody TodoList todoList) {
        if (!id.equals(todoList.getId())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        todoList.setOwner(userService.getLoggedInUser());
        return ResponseEntity.ok(todoListRepository.save(todoList));
    }

    /**
     * Dodawanie nowej listy
     */
    @PostMapping("/todo")
    public TodoList addNewTodoList(@RequestBody TodoList todoList) {
        User loggedInUser = userService.getLoggedInUser();

        if (loggedInUser != null) {
            todoList.setId(null);
            todoList.setItems(Collections.emptyList());
            todoList.setOwner(loggedInUser);

            return todoListRepository.save(todoList);
        } else {
            return null;
        }
    }

    /**
     * Usunięcie listy
     */
    @DeleteMapping("/todo/{id}")
    @PreAuthorize("@todoListAccessService.canUserAccessTodoList(authentication.principal, #id)")
    public ResponseEntity<?> deleteTodoList(@PathVariable("id") Integer id) {
        Optional<TodoList> todoListOptional = todoListRepository.findById(id);
        if (todoListOptional.isPresent()) {
            TodoList todoListToDelete = todoListOptional.get();

            User loggedInUser = userService.getLoggedInUser();
            if (todoListToDelete.getOwner().getId().equals(loggedInUser.getId())) {
                todoListRepository.delete(todoListToDelete);
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            } else {
                return ResponseEntity.notFound().build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
