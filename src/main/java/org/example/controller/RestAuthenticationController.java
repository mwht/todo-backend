package org.example.controller;

import org.example.model.User;
import org.example.repository.RoleRepository;
import org.example.service.JwtService;
import org.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class RestAuthenticationController {

    @Autowired
    JwtService jwtService;

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @RequestMapping(value="/login", method = RequestMethod.POST)
    public Map<String, String> login(@RequestBody User user, HttpServletRequest httpServletRequest, HttpSession session) {
        Map<String, String> result = new HashMap<>();

        if (user.getEmail() != null && user.getPassword() != null) {
            String email = user.getEmail();
            String password = user.getPassword();
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(email, password);

            Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            User loggedInUser = userService.getLoggedInUser();

            result.put("result", "success");
            result.put("session", jwtService.generateJwtToken(loggedInUser.getId(), new Date(System.currentTimeMillis() + 24*3600*1000)));
        } else {
            result.put("result", "failure");
        }
        return result;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpSession session) {
        session.invalidate();
    }

    @RequestMapping(value="/register", method=RequestMethod.POST)
    public User registerUser(@RequestBody User user) {
        user.setId(null);
        user.setUser_id(null);
        return userService.saveUser(user);
    }


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home"); // resources/template/home.html
        return modelAndView;
    }


    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView adminHome() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin"); // resources/template/admin.html
        return modelAndView;
    }

}
